/*
Copyright 2020 Dimitris Mantzouranis

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include QMK_KEYBOARD_H

#define KC_FXME KC_NO
#define _FN WIN_FN

#define KC_WNOT LSG(KC_L)
#define KC_WLCK LSG(KC_S)
#define KC_WMIC LAG(KC_M)
#define KC_MNOT LSG(KC_L) /* there's no macos command for notifications, so just pick one */
#define KC_MLCK LCTL(LGUI(KC_Q))
#define KC_MMIC LAG(KC_M) /* same here */

enum my_keycodes {
    BL_RSTP = SAFE_RANGE,
    BL_SPDU,
    BL_SPDD,
    SWL_OFF
};

const uint16_t PROGMEM keymaps[][MATRIX_ROWS][MATRIX_COLS] = {
    [WIN_BASE] = LAYOUT( \
        LT(_FN, KC_ESC), KC_BRID, KC_BRIU, KC_WNOT, KC_WLCK, BL_DEC, BL_INC, KC_MPRV, KC_MPLY, KC_MNXT, KC_MUTE, KC_VOLD, KC_VOLU, KC_PSCR, KC_WMIC, BL_TOGG, \
        KC_GRV, KC_1, KC_2, KC_3, KC_4, KC_5, KC_6, KC_7, KC_8, KC_9, KC_0, KC_MINS, KC_EQL, KC_BSPC, KC_INS, KC_HOME, KC_PGUP, \
        KC_TAB, KC_Q, KC_W, KC_E, KC_R, KC_T, KC_Y, KC_U, KC_I, KC_O, KC_P, KC_LBRC, KC_RBRC, KC_BSLS, KC_DEL, KC_END, KC_PGDN, \
        KC_ESC, KC_A, KC_S, KC_D, KC_F, KC_G, KC_H, KC_J, KC_K, KC_L, KC_SCLN, KC_QUOT, KC_ENT, \
        KC_LSFT, KC_Z, KC_X, KC_C, KC_V, KC_B, KC_N, KC_M, KC_COMM, KC_DOT, KC_SLSH, KC_RSFT, KC_UP, \
        KC_LCTL, KC_LGUI, KC_LALT, KC_SPC, KC_LALT, KC_RGUI, LT(_FN, KC_RALT), KC_RCTL, KC_LEFT, KC_DOWN, KC_RGHT \
    ),

    [_FN] = LAYOUT( \
        KC_TRNS, KC_F1, KC_F2, KC_F3, KC_F4, KC_F5, KC_F6, KC_F7, KC_F8, KC_F9, KC_F10, KC_F11, KC_F12, KC_TRNS, KC_TRNS, BL_STEP, \
        KC_TRNS, KC_F13, KC_F14, KC_F15, KC_F16, KC_F17, KC_F18, KC_F19, KC_F20, KC_F21, KC_F22, KC_TRNS, KC_TRNS, KC_TRNS, RESET, KC_TRNS, KC_TRNS, \
        KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_PAUSE, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_END, KC_TRNS, KC_TRNS, KC_TRNS, SWL_OFF, KC_TRNS, KC_TRNS, \
        KC_CAPS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_HOME, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, \
        KC_TRNS, KC_TRNS, KC_TRNS, KC_PGDN, KC_PGUP, KC_TRNS, LCTL(KC_LEFT), KC_APPLICATION, LCTL(KC_RIGHT), KC_TRNS, KC_TRNS, KC_TRNS, BL_SPDU, \
        KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, /* < space */ KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, BL_RSTP, BL_SPDD, BL_STEP \
    ),

    [MAC_BASE] = LAYOUT( \
        KC_ESC, KC_BRID, KC_BRIU, KC_MNOT, KC_MLCK, BL_DEC, BL_INC, KC_MPRV, KC_MPLY, KC_MNXT, KC_MUTE, KC_VOLD, KC_VOLU, KC_PSCR, KC_MMIC, BL_TOGG, \
        KC_GRV, KC_1, KC_2, KC_3, KC_4, KC_5, KC_6, KC_7, KC_8, KC_9, KC_0, KC_MINS, KC_EQL, KC_BSPC, KC_INS, KC_HOME, KC_PGUP, \
        KC_TAB, KC_Q, KC_W, KC_E, KC_R, KC_T, KC_Y, KC_U, KC_I, KC_O, KC_P, KC_LBRC, KC_RBRC, KC_BSLS, KC_DEL, KC_END, KC_PGDN, \
        KC_CAPS, KC_A, KC_S, KC_D, KC_F, KC_G, KC_H, KC_J, KC_K, KC_L, KC_SCLN, KC_QUOT, KC_ENT, \
        KC_LSFT, KC_Z, KC_X, KC_C, KC_V, KC_B, KC_N, KC_M, KC_COMM, KC_DOT, KC_SLSH, KC_RSFT, KC_UP, \
        KC_LCTL, KC_LALT, KC_LGUI, KC_SPC, KC_RGUI, KC_RALT, MO(_FN), KC_RCTL, KC_LEFT, KC_DOWN, KC_RGHT \
    ),

};

bool process_record_user(uint16_t keycode, keyrecord_t *record) {
    switch (keycode) {
        case BL_RSTP:
            if (record->event.pressed) {
                led_matrix_step_reverse();
            }
            return false;
        case BL_SPDU:
            if (record->event.pressed) {
                led_matrix_increase_speed();
            }
            return false;
        case BL_SPDD:
            if (record->event.pressed) {
                led_matrix_decrease_speed();
            }
            return false;
        case SWL_OFF:
            if (record->event.pressed) {
                writePin(LED_WIN_PIN, 0);
                writePin(LED_MAC_PIN, 0);
            }
            return false;
        default:
            return true;
    }
}
